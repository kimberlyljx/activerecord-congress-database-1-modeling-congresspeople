require_relative '../../db/config'

class Legislator < ActiveRecord::Base
  validates :title, presence: true
  validates :firstname, presence: true
  validates :lastname, presence: true
  validates :party, presence: true
  validates :state, presence: true
  validates :gender, presence: true
  validates :bioguide_id, presence: true
  validates :govtrack_id, presence: true
  validates :crp_id, presence: true

  def name
    "#{firstname} #{middlename} #{lastname} #{name_suffix}"
  end


  def self.print_senators_in_state(state)
    Legislator.where(state: state, title: 'Sen').order(lastname: :desc).each do |legislator|
      puts "#{legislator.name}"
    end
  end

  def self.print_rep_in_state(state)
    Legislator.where(state: state, title: 'Rep').order(lastname: :desc).each do |x|
      puts "#{x.name}"
    end
  end

  def self.print_senator_gender_percentage(input)
    number =  Legislator.where(gender: input, in_office: 1, title: 'Sen').count
    total_number = Legislator.where(in_office: 1, title: 'Sen').count
    puts "#{input} Senators in total = #{number}." "In percentage = #{number*100/total_number}%"
  end

  def self.print_rep_gender_percentage(input)
    number =  Legislator.where(gender: input, in_office: 1, title: 'Rep').count
    total_number = Legislator.where(in_office: 1, title: 'Rep').count
    puts "#{input} Reps in total = #{number}." "In percentage = #{number*100/total_number}%"
  end

  def self.print_rep_gender_percentage(input)
    number =  Legislator.where(gender: input, in_office: 1, title: 'Rep').count
    total_number = Legislator.where(in_office: 1, title: 'Rep').count
    puts "#{input} Reps in total = #{number}." "In percentage = #{number*100/total_number}%"
  end

  def self.print_list
    puts "Here is your list of active senators and reps in each state in descending order"
    senator = Legislator.select(:state).where(in_office: 1, title: 'Sen').group("state").count
    reps =  Legislator.select(:state).where(in_office: 1, title: 'Rep').group("state").count
    sorted_reps = reps.sort_by {|k,v| v}.reverse
    p sorted_reps
    sorted_reps.each do |k,v|
      p "#{k}" "   Representatives = #{v}" "   Senators = #{senator[k]}"
    end
  end
end