require_relative './models/legislator'
require 'sqlite3'
require 'byebug'

class State
  def self.run
    puts "Which state?"
    state = gets.chomp
    puts "What title? Enter number:"
    puts "1. Senators"
    puts "2. Representatives"
    title = gets.chomp
    case title
    when "1"
      Legislator.print_senators_in_state(state)
    when "2"
      Legislator.print_rep_in_state(state)
    else
      puts "Invalid entry"
    end

  end
end

class Gender
  def self.run
  puts "which title?"
  puts "1. Senators"
  puts "2. Representatives"
  title = gets.chomp.upcase
  puts "Which Gender?"
  input = gets.chomp.upcase
    case title
      when "1"
        Legislator.print_senator_gender_percentage(input)
      when "2"
        Legislator.print_rep_gender_percentage(input)
      else
        puts "Invalid entry"
    end
  end
end

class List
  def self.run
    Legislator.print_list
  end
end

class Count
  def self.run
    puts "Senators: " + Legislator.all.where(title: "Sen").count.to_s
    puts "Representatives: " + Legislator.all.where(title: "Rep").count.to_s
  end
end

class CountInactive
  def self.run
    user = Legislator.where(in_office: 0)
    user.each do |x|
      x.destroy
    end
    puts "Senators: " + Legislator.all.where(title: "Sen").count.to_s
    puts "Representatives: " + Legislator.all.where(title: "Rep").count.to_s
  end
end

class App
  def self.run
    puts "Choose number?"
    puts "1. By State"
    puts "2. By Gender"
    puts "3. By List of senators and reps per state"
    puts "4. Count the number of Senators and Representatives"
    puts "5. Delete congresspeople who are not active in office"

    input = gets.chomp
    case input
    when "1"
      State.run
    when "2"
      Gender.run
    when "3"
      List.run
    when "4"
      Count.run
    when "5"
      CountInactive.run

    end
  end
end

App.run


