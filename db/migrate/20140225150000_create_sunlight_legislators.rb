require_relative '../config'

# this is where you should use an ActiveRecord migration to

class CreateSunlightLegislators < ActiveRecord::Migration
  def change
    create_table :legislators do |t|
      t.string :title, null: false
      t.string :firstname, null: false
      t.string :middlename
      t.string :lastname, null: false
      t.string :name_suffix
      t.string :nickname
      t.string :party, null: false
      t.string :state, null: false
      t.string :district
      t.integer :in_office
      t.string :gender, null: false
      t.string :phone
      t.string :fax
      t.string :website
      t.string :webform
      t.string :congress_office
      t.string :bioguide_id
      t.integer :votesmart_id
      t.string :fec_id
      t.integer :govtrack_id
      t.string :crp_id
      t.string :twitter_id
      t.string :congresspedia_url
      t.string :youtube_url
      t.string :facebook_id
      t.string :official_rss
      t.string :senate_class
      t.string :birthdate

    end
  end
end
